package fr.ralmn.jira;

import java.net.URISyntaxException;

/**
 * @author ralmn
 */
public class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static JavaExceptionJiraReport javaExceptionJiraReport;

    public static void registerExceptionHandler(JavaExceptionJiraReport javaExceptionJiraReport) {
        ExceptionHandler.javaExceptionJiraReport = javaExceptionJiraReport;
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        System.setProperty("sun.awt.exception.handler", ExceptionHandler.class.getName());
    }

    public void uncaughtException(Thread t, Throwable e) {
        handle(e);
    }

    public void handle(Throwable throwable) {
        javaExceptionJiraReport.reportBug(throwable);
    }

}