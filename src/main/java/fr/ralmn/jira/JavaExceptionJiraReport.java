package fr.ralmn.jira;

import com.atlassian.jira.rest.client.api.GetCreateIssueMetadataOptionsBuilder;
import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.api.domain.*;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * @author ralmn
 */
public class JavaExceptionJiraReport {


    private String user;
    private String pwd;
    private String projectKey;
    private ArrayList<String> customfieldsName;
    private ArrayList<CustomFieldOption> customfieldsOptions;
    private String urlJira;

    public JavaExceptionJiraReport(String urlJira,String user, String pwd, String projectKey, String... customfieldsName){
        this.user = user;
        this.pwd = pwd;
        this.projectKey = projectKey;
        this.customfieldsName = new ArrayList<String>();
        for (String s : customfieldsName) {
            this.customfieldsName.add(s);
        }

        this.urlJira = urlJira;
        this.customfieldsOptions = new ArrayList<CustomFieldOption>();
        ExceptionHandler.registerExceptionHandler(this);
    }

    private static void onError() {
        File file = null;
        file.getName();
    }

    public void reportBug(Throwable t) {
        String description = t.toString() + "\n";
        String name = t.toString() + " - " + t.getStackTrace()[0].toString();

        for (StackTraceElement stackTraceElement : t.getStackTrace()) {
            description += stackTraceElement.toString();
            description += "\n";
        }

        if (t.getCause() != null) {


            description += "Caused by : ";
            for (StackTraceElement stackTraceElement : t.getCause().getStackTrace()) {
                description += stackTraceElement.toString();
                description += "\n\r";
            }
        }


        final JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        final URI jiraServerUri;
        try {
            jiraServerUri = new URI(urlJira);
        } catch (URISyntaxException e) {
            System.out.println("Url ERROR");
            return;
        }
        final JiraRestClient restClient = factory.createWithBasicHttpAuthentication(jiraServerUri, user, pwd);

        IssueRestClient issueClient = restClient.getIssueClient();

        Iterable<CimProject> metadataProjects = issueClient.getCreateIssueMetadata(
                new GetCreateIssueMetadataOptionsBuilder().withProjectKeys(projectKey).withExpandedIssueTypesFields().build()
        ).claim();

        final CimProject p = metadataProjects.iterator().next();

        SearchResult searchResult = restClient.getSearchClient().searchJql("summary%20~%20\"" + t.toString() + "\"").claim();
        for (BasicIssue basicIssue : searchResult.getIssues()) {
            Issue claim = issueClient.getIssue(basicIssue.getKey()).claim();
            if (claim.getSummary().equalsIgnoreCase(name)) {
                System.out.println("\"" + name + "\n already report (" + claim.getKey() + ")");
                try {
                    restClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
        }

        final CimIssueType issueType = EntityHelper.findEntityById(p.getIssueTypes(), (long) 1);

        if (issueType != null) {
            User u = restClient.getUserClient().getUser(user).claim();
            IssueInputBuilder ib = new IssueInputBuilder(p, issueType, name);

            ib.setDescription(description);

            for (CimFieldInfo cimFieldInfo : issueType.getFields().values()) {
                System.out.println(cimFieldInfo.getName() + " - " + cimFieldInfo.getId());
                if (cimFieldInfo.getAllowedValues() != null)
                    for (Object o : cimFieldInfo.getAllowedValues()) {
                        if (o instanceof CustomFieldOption) {
                            CustomFieldOption c = (CustomFieldOption) o;
                            if (customfieldsName.contains("customfield_" + c.getId())) {
                                customfieldsOptions.add(c);
                                break;
                            }
                        }
                    }
            }

            for (CustomFieldOption customfieldsOption : customfieldsOptions) {
                ib.setFieldValue("customfield_" + customfieldsOption.getId(), customfieldsOption);
            }


            BasicIssue issue = issueClient.createIssue(ib.build()).claim();
            System.out.println(issue.toString());
            try {
                restClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}