JavaExceptionJiraReport
=========
_By [ralmn](http://twitter.com/ralmn45)_


Système de report d'erreur sur JIRA

Report de bug : [jira](http://jira.bombercraft.fr/browse/JEJR)

Maven
-------

```
<repositories>
    ...
	<repository>
		<id>ci.ralmn</id>
		<url>http://ci.ralmn.fr:8086/nexus/content/groups/public</url>
	</repository>
	...
</repositories>

```


```
<dependencies>
    <dependency>
		...
		<groupId>fr.ralmn.jira</groupId>
		<artifactId>JavaExceptionJiraReport</artifactId>
		<version>1.0-SNAPSHOT</version>
	</dependency>
	...
</dependencies>
```
